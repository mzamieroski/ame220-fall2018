# Assignment 1
**Due Sep 5<sup>th</sup> before class**

## Task 

Submit a 1 page resume as a single ```index.html``` file.

## Grading
1. Consistent html tagging.
2. Thumbnail photo
3. Phone, email, address should be links.
4. Should have atleast 4 sections. E.g. "Objective", "Education" (use tables), "Experience" (unordered list of paragraphs), "Skills", "References" (numbered list).

